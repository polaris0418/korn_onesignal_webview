using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpControl : MonoBehaviour
{
    bool isPopupShow = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        popupPanel.SetActive(isPopupShow);
    }

    public GameObject popupPanel;
    // close popup panel
    public void OnClosePopUp(){
        isPopupShow = false;
        //popupPanel.SetActive(isPopupShow);
    }
    public void OnShowPopUp(){
        isPopupShow = true;
        //popupPanel.SetActive(isPopupShow);
    }
    // Visit google play store
    public void OnRateUs(){
        Application.OpenURL("https://play.google.com");
    }

}
