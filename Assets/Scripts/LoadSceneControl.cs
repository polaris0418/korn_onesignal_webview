using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSceneControl : MonoBehaviour
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield  return new WaitForSeconds(3f);
        GameManager.Instance.GoToNextScene();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
