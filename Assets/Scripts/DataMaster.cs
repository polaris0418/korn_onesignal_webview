using UnityEngine;
 using System.Collections;
 using System;
 
 public class DataMaster : MonoBehaviour
 {
     DateTime currentDate;
     DateTime oldDate;
     void Start()
     {
         //PlayerPrefs.DeleteAll();
         //Store the current time when it starts
         currentDate = System.DateTime.Now;
 
         
         //Convert the old time from binary to a DataTime variable
         if(PlayerPrefs.GetInt("first launch", 1) == 1){
            PlayerPrefs.SetInt("first launch", 0);
           
            //Savee the current system time as a string in the player prefs class
            PlayerPrefs.SetString("sysString", System.DateTime.Now.ToBinary().ToString());
            print("Saving this date to prefs: " + System.DateTime.Now);

         }else{
             //Grab the old time from the player prefs as a long
            long temp = Convert.ToInt64(PlayerPrefs.GetString("sysString"));
            DateTime oldDate = DateTime.FromBinary(temp);
            print("oldDate: " + oldDate);
            //Use the Subtract method and store the result as a timespan variable
            TimeSpan difference = currentDate.Subtract(oldDate);
            print("Difference: " + difference);

            //Pop up Rate Us dialog
            
            if(difference.TotalHours > 24){
                Debug.Log(difference.TotalHours);
                GameManager.Instance.ShowRateUS();
            }
    
         }
         
     }
 
     void OnApplicationQuit()
     {
         //Savee the current system time as a string in the player prefs class
         PlayerPrefs.SetString("sysString", System.DateTime.Now.ToBinary().ToString());
 
         print("Saving this date to prefs: " + System.DateTime.Now);
     }
 
 }